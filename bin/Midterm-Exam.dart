import 'dart:io';
import 'dart:math';
import 'Midterm-Exam.dart';

String s_token = '231*+9-';
String s_ITP = '231*+9-';
String s_EOP = '231*+9-';
void main(List<String> args) {
  ex122token();
  ex123infixtopostfix();
  ex124evaofpostfix();
}

void ex122token() {
  var token = s_token.split(' '); // keep 231*+9- in token
  for (int i = 0; i < token.length; i++) {
    token.remove(''); // [231*+9-]
  }
  print('Tokenizing : $token ');
}

void ex123infixtopostfix() {
  var st = List.empty(growable: true); //[]
  var ans = List.empty(growable: true); //[]
  var prec = {'+': 1, '-': 1, '*': 2, '/': 2,'%':3, '(': 4, ')': 4}; // Precedence setting
  List<String> check = ["+", "-", "^", "*", "/"];//check operator
  for (var i = 0; i < s_ITP.length; i++) {
    if (check.contains(s_ITP[i])) {
      while (st.isNotEmpty &&
          st[st.length - 1] != "(" &&
          prec[s_ITP[i]]! <= prec[st[st.length - 1]]!) {
        ans.add(st[st.length - 1]);
        st.removeLast();
      }
      st.add(s_ITP[i]);
    } else if (s_ITP[i] == "(" || s_ITP[i] == ")") {
      if (s_ITP[i] == "(") {
        st.add(s_ITP[i]);
      }
      if (s_ITP[i] == ")") {
        while (st[st.length - 1] != "(") {
          ans.add(st[st.length - 1]);
          st.removeLast();
        }
        st.removeLast();
      }
    } else {
      if (int.parse(s_ITP[i]) is int) {
        ans.add(s_ITP[i]);
      }
    }
  }
  while (st.isNotEmpty) {
    ans.add(st[st.length - 1]);
    st.removeLast();
  }
  print('InfixToPostfix : $ans');

}

void ex124evaofpostfix() {
  var st = List.empty(growable: true); //[]
  var ans = List.empty(growable: true);
  double num;
  for (int i = 0; i < s_EOP.length; i++) {
    if (s_EOP[i] == "0" ||
        s_EOP[i] == "1" ||
        s_EOP[i] == "2" ||
        s_EOP[i] == "3" ||
        s_EOP[i] == "4" ||
        s_EOP[i] == "5" ||
        s_EOP[i] == "6" ||
        s_EOP[i] == "7" ||
        s_EOP[i] == "8" ||
        s_EOP[i] == "9") {
      switch (s_EOP[i]) {
        case "0":
          {
            num = 0;
            st.add(num);
            ans = st;
          }
          break;
        case '1':
          {
            num = 1;
            st.add(num);
            ans = st;
          }
          break;
        case '2':
          {
            num = 2;
            st.add(num);
            ans = st;
          }
          break;
        case '3':
          {
            num = 3;
            st.add(num);
            ans = st;
          }
          break;
        case '4':
          {
            num = 4;
            st.add(num);
            ans = st;
          }
          break;
        case '5':
          {
            num = 5;
            st.add(num);
            ans = st;
          }
          break;
        case '6':
          {
            num = 6;
            st.add(num);
            ans = st;
          }
          break;
        case '7':
          {
            num = 7;
            st.add(num);
            ans = st;
          }
          break;
        case '8':
          {
            num = 8;
            st.add(num);
            ans = st;
          }
          break;
        case '9':
          {
            num = 9;
            st.add(num);
            ans = st;
          }
          break;
      }
    } else {
      double r , l ;
      r = ans.last;
      ans.removeLast();
      l = ans.last;
      ans.removeLast();
      double re = 0;
      switch(s_EOP[i]){
        case '+':{
          re = (l + r);
        }
        break;
        case '-':{
          re = (l - r);
        }
        break;
        case '*':{
          re = (l * r);
        }
        break;
        case '/':{
          re = (l / r);
        }
        break;
        case '%':{
          re = (l % r);
        }
        break;

      }
      ans.add(re);
      
    }
  }
  print('Evaluate of postfix : $ans');
}
